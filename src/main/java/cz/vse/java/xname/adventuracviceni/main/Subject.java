package cz.vse.java.xname.adventuracviceni.main;

public interface Subject {

    void register(Observer observer);
    void unregister(Observer observer);

    void notifyObservers();
}
