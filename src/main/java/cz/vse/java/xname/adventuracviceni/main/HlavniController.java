package cz.vse.java.xname.adventuracviceni.main;

import cz.vse.java.xname.adventuracviceni.logika.IHra;
import cz.vse.java.xname.adventuracviceni.logika.PrikazJdi;
import cz.vse.java.xname.adventuracviceni.logika.Prostor;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Point2D;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;

import java.util.HashMap;
import java.util.Map;

public class HlavniController extends GridPane implements Observer {
    @FXML
    public TextArea vystup;
    @FXML
    public TextField vstup;
    @FXML
    public ListView seznamVychodu;
    @FXML
    public ImageView hrac;

    private IHra hra;
    private Map<String, Point2D> souradniceProstoru = new HashMap<>();

    /**
     * Uloží instanci hry do kontroleru a vrátí uvítání
     * @param hra
     */
    public void inicializace(IHra hra) {
        this.hra = hra;
        vystup.setEditable(false);
        vypisUvitani();
        vstup.requestFocus();

        souradniceProstoru.put("domecek", new Point2D(27,137));
        souradniceProstoru.put("les", new Point2D(120,88));
        souradniceProstoru.put("hluboky_les", new Point2D(200,133));
        souradniceProstoru.put("chaloupka", new Point2D(293,90));
        souradniceProstoru.put("jeskyne", new Point2D(201,231));

        hra.getHerniPlan().register(this);

        update();
    }

    /**
     * Vypíše do výstupního elementu uvítání
     */
    public void vypisUvitani() {
        vystup.appendText(hra.vratUvitani()+"\n\n");
    }

    /**
     * Zpracování vstupního textu, předání hře a výpis výsledku příkazu
     * @param actionEvent
     */
    public void zpracujVstup(ActionEvent actionEvent) {
       zpracujPrikaz(vstup.getText());
    }

    private void zpracujPrikaz(String prikaz) {
        String vysledek = hra.zpracujPrikaz(prikaz);
        vystup.appendText(vysledek+"\n");
        vstup.setText("");

        if(hra.konecHry()) {
            vystup.appendText("\n"+hra.vratEpilog());
            vstup.setDisable(true);
            seznamVychodu.setDisable(true);
        }
    }

    @Override
    public void update() {
        seznamVychodu.getItems().clear();
        seznamVychodu.getItems().addAll(hra.getHerniPlan().getAktualniProstor().getVychody());

        hrac.setX(souradniceProstoru.get(hra.getHerniPlan().getAktualniProstor().getNazev()).getX());
        hrac.setY(souradniceProstoru.get(hra.getHerniPlan().getAktualniProstor().getNazev()).getY());
    }

    public void jdiVychod(MouseEvent mouseEvent) {
        Prostor cilenyProstor = (Prostor) seznamVychodu.getSelectionModel().getSelectedItem();
        zpracujPrikaz(PrikazJdi.getNAZEV()+" "+cilenyProstor.getNazev());
    }
}
