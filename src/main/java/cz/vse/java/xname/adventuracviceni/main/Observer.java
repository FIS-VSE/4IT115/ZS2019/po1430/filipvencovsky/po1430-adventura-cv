package cz.vse.java.xname.adventuracviceni.main;

public interface Observer {

    /**
     * Metoda se volá při změně pro aktualizaci pozorovatele
     */
    void update();
}
